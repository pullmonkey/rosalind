n = ARGV[0].to_i

class Array
  # use a base to multiply against to add in negatives
  # [-1,1,1]
  def negatives
    bases = []
    (1..self.size).each do |num_negs|
      base = [1] * self.size
      num_negs.times do |pos|
        base[pos] = -1
      end
      bases << base
    end
    to_ret = []
    bases.each do |base|
      base.permutation.each do |base_perm|
        np = self.dup
        np.each_with_index do |x,i|
          np[i] = x * base_perm[i]
        end
        to_ret << np
      end
    end
    to_ret
  end
end

perms = (1..n).to_a.permutation.to_a

perms.dup.each do |perm|
  perm.negatives.each do |p|
    perms << p
  end
end
perms = perms.uniq
puts perms.size
perms.each do |perm|
  puts perm.join(" ")
end
