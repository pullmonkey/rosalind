$fib_vals = {}

def fib(n,m)
  key = "#{n}.#{m}"
  return $fib_vals[key] if $fib_vals.has_key?(key)
  return 1 if n <= 2
  answer = fib(n-1,m) + fib(n-2,m)
  answer -= fib(n-m-1,m) if n > m
  $fib_vals[key] = answer
  answer
end

puts fib(88,18)
#puts fib(87,20)
