lines = File.open("rosalind_1d.txt").readlines
genome = lines.first.strip 

k, l, t = lines[1].strip.split(" ")
k = k.to_i
l = l.to_i
t = t.to_i

matches = {}

genome.size.times do |offset|
  pattern = genome[offset..(offset + k - 1)]
  matches[pattern] = genome.enum_for(:scan, /#{pattern}/).map{ Regexp.last_match.begin(0)}
end
matches = matches.select do | pattern, matches | 
  pattern.size == k && 
  matches.size >= t
end

final_matches = []
matches.each do |m|
  pattern = m[0]
  offsets = m[1].sort

  if offsets.last - offsets.first <= l
    final_matches << pattern
    next
  end

  keep_trying = true
  # first and last positions too far apart
  (offsets.size - t).times do |x|
    break unless keep_trying
    sub_offsets_test1 = offsets[x..-1]
    if sub_offsets_test1.last - sub_offsets_test1.first >= t
      final_matches << pattern
      keep_trying = false
      next
    end
    sub_offsets_test2 = offsets[0..-x]
    if sub_offsets_test2.last - sub_offsets_test2.first >= t
      final_matches << pattern
      keep_trying = false
      next
    end
  end

end
puts final_matches
