lines = File.open("sample.txt").readlines
genome = lines.first.strip 

k, l, t = lines[1].strip.split(" ")
k = k.to_i
l = l.to_i
t = t.to_i

matches = {}

genome.size.times do |offset|
  pattern = genome[offset..(offset + k - 1)]
  matches[pattern] = genome.enum_for(:scan, /#{pattern}/).map{ Regexp.last_match.begin(0)}
end
matches = matches.select do | pattern, matches | 
  pattern.size == k && 
  matches.size == t &&
  matches.sort.last - matches.sort.first <= l
end

puts matches.map{|m| m[0]}.inspect
