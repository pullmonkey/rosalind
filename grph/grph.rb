#fs = File.open("sample.txt").readlines.map(&:strip)
#fs = File.open("sample2.txt").readlines.map(&:strip)
fs = File.open("rosalind_grph.txt").readlines.map(&:strip)

$node_values = {}
$prefixes    = {}
$suffixes    = {}

def update_data_for_key(key)
  prefix = $node_values[key]["dna"][0..2]
  suffix = $node_values[key]["dna"][-3..-1]
  $node_values[key]["prefix"] = prefix
  $node_values[key]["suffix"] = suffix
  $prefixes[suffix] ||= [] # so that we, at the very least, get an empty array instead of nil
  $prefixes[prefix] ||= []
  $prefixes[prefix] << key
  $suffixes[prefix] ||= [] # just to allow there to be something
  $suffixes[suffix] ||= []
  $suffixes[suffix] << key
end

key = nil
fs.each do |l|
  if l =~ /^>/
    # solve last key's suffices and prefixes
    update_data_for_key(key) if key
    key = l.gsub(/^>/,"")
  else
    $node_values[key] ||= {}
    $node_values[key]["dna"] ||= ""
    $node_values[key]["dna"] += l
  end
end
update_data_for_key(key) if key

edges = []
$node_values.each do |node, data|
  # collects in -> out as [in,out]
  node_edges = []
  # for each node, look for all edges we go to
  # our suffix is their prefix
  $prefixes[data["suffix"]].each do |out_node|
    node_edges << [node, out_node] unless node == out_node
  end
  # for each node, look for all edges we come from
  # our prefix is their suffix
  $suffixes[data["prefix"]].each do |in_node|
    node_edges << [in_node, node] unless node == in_node
  end
  edges << node_edges unless node_edges.empty?
end

edges.flatten(1).uniq.each do |edge|
  puts "#{edge[0]} #{edge[1]}"
end
