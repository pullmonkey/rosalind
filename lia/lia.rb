file = ARGV[0]

# k is generation
# N is num orgs of Aa Bb we are looking for in our probability
k,n = File.open(file).readlines[0].split(" ").map(&:to_i)

# we are start in 0th generation with Tom (Aa Bb)
# Tom has 2 children in 1st gen, each of which have 2 children
# Each child (on and on) mates with an org (Aa Bb)

# gen 0
# we have Tom (Aa Bb)
# Aa Bb with Aa Bb => AB ab  X AB ab
# Tom mates with Aa Bb to have 2 children
#      AB      ab
# AB  AA BB   Aa Bb
# ab  Aa Bb   aa bb
# 50% in gen 1, two off spring

# aa bb X Aa Bb ....
#      ab      ab
# AB  Aa Bb  Aa Bb
# ab  aa bb  aa bb

# AA BB X Aa Bb
#      AB    AB
# AB  AA BB  AA BB
# ab  Aa Bb  Aa Bb

# can this happen?
# AA bb X Aa Bb
#      Ab    Ab
# AB  AA Bb  AA Bb
# ab  Aa bb  Aa bb

# when Aa Bb mates with Aa Bb => 0.5
# ----------------------------------
# Aa Bb X Aa Bb => 0.5 Aa Bb
# aa bb X Aa Bb => 0.5
# AA BB X Aa Bb => 0.5 


