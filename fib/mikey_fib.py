#!/usr/bin/python
import sys
N = int(sys.argv[1])
m = int(sys.argv[2])

def f(n):
  if n == 0: return 0
  elif n == 1: return 1
  else: return f(n-1) + m*f(n-2)

o = f(N)
print o
