class Array
  def sum
    to_ret = 0
    self.each do |x|
      to_ret += x
    end
    to_ret
  end
end

def fib(n,k)
  # #Adults:#Babies
  return "0:1" if n <= 1
  last = fib(n-1,k).split(":")
  num_babies = k*last.first.to_i
  num_adults = last.first.to_i + last.last.to_i
  return "#{num_adults}:#{num_babies}"
end

puts fib(36,3).split(":").map(&:to_i).sum
