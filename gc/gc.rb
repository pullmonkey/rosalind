#fasta = File.open("sample.txt").readlines
fasta = File.open("rosalind_gc.txt").readlines

fastas = {}
fasta_id = nil

def gc_content(line)
  line = line.strip
  100.0 * line.split("").select{|c| ["G", "C"].include?(c)}.size.to_f / line.size.to_f
end

long_line = ""
fasta.each do |line|
  if line =~ /^>(.*)/
    fastas[fasta_id] = gc_content(long_line) if fasta_id
    fasta_id = $1
    long_line = ""
    next
  else
    long_line += line.strip
  end
end

puts fastas.inspect
max_val = fastas.values.max
fasta = fastas.to_a.find{|f| f[1] == max_val}
puts fasta.join("\n")
