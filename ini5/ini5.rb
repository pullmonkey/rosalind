require 'active_support/all'
file = ARGV[0]

lines = File.open(file).readlines
lines.each_with_index do |line, i|
  next if i.even?
  puts line
end
