class Genome
  def initialize(text)
    @text = text
  end
  
  def reverse_complement
    @text.reverse.gsub("A", "t").gsub("T","a").gsub("G","c").gsub("C","g").upcase
  end
end

# reverse complement
#lines = File.open("sample.txt").readlines
# ACCGGGTTTT
lines = File.open("rosalind_1b.txt").readlines

text = lines[0].strip

g = Genome.new(text)
puts g.reverse_complement
