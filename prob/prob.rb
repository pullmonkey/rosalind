#lines = File.open("sample.txt").readlines.map(&:strip)
lines = File.open("rosalind_prob.txt").readlines.map(&:strip)

s = ""
a = []
lines.each do |l|
  s += l if l =~ /^[^\d]/
  if l =~ /^\d/
    a += l.split(" ").map(&:to_f)
  end
end
a = a.flatten

probs = []
a.each do |x|
  prob_gc = x * 0.5
  prob_at = (1-x) * 0.5

  prob = 0.0
  s.split("").each do |c|
    prob += Math.log10(["A","T"].include?(c) ? prob_at : prob_gc)
  end
  probs << prob
end
puts probs.join(" ")
