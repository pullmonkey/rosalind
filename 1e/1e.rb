lines = File.open("1e/rosalind_1e.txt").readlines

genome = lines.first.strip

skews = []

def skew(prefix, skews)
  previous_skew = skews.last || 0
  val = 0
  val = -1 if prefix == "C"
  val =  1 if prefix == "G"
  previous_skew + val
end

def prefix(i, genome)
  return "" if i == 0
  genome[i-1..i-1]
end

skews_by_value = {}
(0..genome.size).each do |i|
  skew_val = skew(prefix(i, genome), skews)
  skews << skew_val
  skews_by_value[skew_val] ||= []
  skews_by_value[skew_val] << i 
end

puts skews_by_value[skews_by_value.keys.min]
