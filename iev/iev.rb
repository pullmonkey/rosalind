#data = File.open("sample.txt").readlines.map(&:strip)[0].split(" ").map(&:to_i)
data = File.open("rosalind_iev.txt").readlines.map(&:strip)[0].split(" ").map(&:to_i)

probs = [1.0, 1.0, 1.0, 0.75, 0.5, 0.0]

sum = 0.0
data.each_with_index do |d,i|
  sum += d * 2.0 * probs[i] 
end

puts sum
