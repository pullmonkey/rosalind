class Genome
  attr_reader :counts

  def initialize(pattern, text)
    @pattern = pattern
    @text = text
  end

  def pattern_positions
    match_positions = []
    str = @text
    offset = 0
    while(match = str.match(/#{@pattern}/)) 
      pos = Regexp.last_match.begin(0) 
      str = str[pos+1..-1]
      match_positions << offset + pos
      offset += pos + 1
    end
    return match_positions
  end
end

#lines = File.open("sample.txt").readlines
# 1 3 9
lines = File.open("rosalind_1c.txt").readlines

pattern = lines[0].strip
text    = lines[1].strip

g = Genome.new(pattern, text)
# damn rosalind didnt accept my answer b/c of commas ...
puts g.pattern_positions.join(" ")
