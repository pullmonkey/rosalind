ACGTTGCATGTCGCATGATGCATGAGAGCT

For GATG:
ACGTTGCATGTCGCATGATGCATGAGAGCT
  GATG
      GATG
             GATG
                GATG?
                    GATG


For ATGC:
ACGTTGCATGTCGCATGATGCATGAGAGCT
   ATGC
       ATGC
              ATGC
                 ATGC?
                     ATGC

For ATGT:
ACGTTGCATGTCGCATGATGCATGAGAGCT
ATGT
       ATGT?
              ATGT
                 ATGT
                     ATGT
 
For ATGA:
ACGTTGCATGTCGCATGATGCATGAGAGCT
       ATGA
              ATGA?
                 ATGA
                     ATGA?
                       ATGA

For CTTG:
ACGTTGCATGTCGCATGATGCATGAGAGCT
  CTTG
      CTTG
             CTTG
                    CTTG
