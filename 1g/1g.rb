class Genome
  attr_reader :counts

  def initialize(text, k, d)
    @text = text
    @k = k
    @d = d
    @bases = ["A","C","G","T"]
    @counts = {}
    @original_mutators = []
    puts "I: Created Genome with #{text.size} text size, k=#{k} and d=#{d}"
  end

  def mutate(kmers, num_mutations)
    puts "M: mutating our kmers at num mutations: #{num_mutations}"
    kmers.map do |kmer|
      mutate_single(kmer, num_mutations)
    end.flatten.uniq
  end

  def mutate_single(kmer, num_mutations, original_kmer=nil)
    @original_mutators = [] if original_kmer.nil?
    mutations = []
    kmer.size.times do |pos_to_mutate|
      @bases.each do |base|
        arr = kmer.split("")
        arr[pos_to_mutate] = base
        mutation = arr.join("")
        unless @original_mutators.include?(mutation)
          @counts[mutation] ||= 0
          @counts[mutation] += 1
          @original_mutators << mutation
        end
        mutations << mutation
      end
    end
    return mutations if num_mutations == 1
    mutations.map{|m| mutate_single(m, num_mutations - 1, original_kmer || kmer)}.flatten
  end

  def base_kmers
    return @base_kmers unless @base_kmers.nil?
    puts "BK: generating base kmers ..."
    @base_kmers = []
    (0..@text.size-@k).each do |pos|
      @base_kmers << @text[pos..(pos + @k-1)]
    end
    return @base_kmers
  end

  def mutate_kmers
    puts "MK: mutating our base kmers ..."
    mutate(base_kmers, @d)
  end

  def find_most_frequent
    puts "FMF: finding most frequent .."
    mutate_kmers
    max_val = @counts.values.max
    puts @counts.to_a.sort{|a,b| a[1] <=> b[1]}
    good_kmers = @counts.to_a.select{|k| k[1] == max_val}.map{|k| k[0]}
    puts good_kmers.inspect
  end

end

#lines = File.open("extra_dataset.txt").readlines
# ["GCACACAGAC", "GCGCACACAC"]
#lines = File.open("rosalind_1g.sample.txt").readlines
# ["ATGT", "GATG", "ATGC"]
lines = File.open("rosalind_1g.txt").readlines

text = lines[0].strip
k, d = lines[1].strip.split(" ").map(&:to_i)

g = Genome.new(text,k,d)
g.find_most_frequent
#puts g.counts["GCACACAGAC"]
#puts g.counts["GCGCACACAC"]

#puts g.mutate_single("GCACACAGAC", 2).uniq.inspect 
#puts g.mutate_single("GAC", 2).uniq.inspect 
