#!/usr/bin/python
import re
import random
from collections import Counter

# Read in the dataset
def get_data(filename):
  f = open("/home/mlbernauer/Downloads/" + filename, "rb")
  data = f.read().split("\n")
  genome = data[0]
  k = int(data[1].split(" ")[0])
  d = int(data[1].split(" ")[1])
  return (genome, k, d)

# Function to calculate the number of times a pattern matches within the genome
def get_score(pat, genome, k, d):
  match_score = 0
  for i in range(len(genome)-k+1):
    mismatch = 0
    gpat = genome[i:i+k]
  # print gpat
    for j in range(k):
      if pat[j] != gpat[j]: mismatch += 1
    if mismatch <= d: match_score += 1
  return pat, match_score

def mutate_pattern(pat, genome, k, d, idx, matches, max_score):
  letters = "GCAT"
  idx += 1
  while idx < k:
    p = list(pat)
    for l in letters:
      p[idx] = l
      pat = "".join(p)
      score = get_score(pat, genome, k, d)[1]
      matches[pat] = score
      mutate_pattern(pat, genome, k, d, idx, matches, max_score)
    idx += 1
#  return (successful_mutations.items(), max_score)
    
def get_frequent_mismatch(genome, k, d):
  kmers = list(set([m.groups()[0] for m in re.finditer(r'(?=(\w{%d}))' % k, genome)]))

  freq_mismatches = {}
#  for i in kmers:
# print "K-mer: %s" % i
  p = "".join([" "]*k)
  mutate_pattern(p, genome, k, d, -1, freq_mismatches, -1)


  return Counter(freq_mismatches).most_common()



g = "ACGTTGCATGTCGCATGATGCATGAGAGCT"
k = 4
d = 1
print get_frequent_mismatch(g, k, d)
