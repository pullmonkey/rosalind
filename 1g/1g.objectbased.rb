DO_TESTS = false
class Genome

  def initialize(text, k, d)
    @text = text
    @k = k
    @d = d
    @bases = ["A","C","G","T"]
    puts "I: Created Genome with #{text.size} text size, k=#{k} and d=#{d}"
  end

  def mutate(kmers, num_mutations)
    puts "M: mutating our kmers at num mutations: #{num_mutations}"
    kmers.map do |kmer|
      mutate_single(kmer, num_mutations)
    end.flatten.uniq
  end

  def mutate_single(kmer, num_mutations)
    mutations = []
    kmer.size.times do |pos_to_mutate|
      @bases.each do |base|
        arr = kmer.split("")
        arr[pos_to_mutate] = base
        mutations << arr.join("")
      end
    end
    return mutations.uniq if num_mutations == 1
    mutations.uniq.map{|m| mutate_single(m, num_mutations - 1)}.flatten.uniq
  end

  def base_kmers
    return @base_kmers unless @base_kmers.nil?
    puts "BK: generating base kmers ..."
    @base_kmers = []
    (0..@text.size-@k).each do |pos|
      @base_kmers << @text[pos..(pos + @k-1)]
    end
    return @base_kmers
  end

  def mutated_kmers
    puts "MK: mutating our base kmers ..."
    mutate(base_kmers, @d)
  end

  def count_frequencies(all_kmers)
    puts "CF: counting frequencies of #{all_kmers.size} kmers ..."
    kmers = {}
    all_kmers.each do |pattern|
      base_kmers.each do |to_compare|
        num_mismatches = 0
        pattern.split("").each_with_index do |p,i|
          num_mismatches += 1 if p != to_compare[i..i] 
          break if num_mismatches > @d
        end
        next if num_mismatches > @d
        kmers[pattern] ||= 0
        kmers[pattern] += 1
      end
    end
    kmers
  end

  def find_most_frequent
    puts "FMF: finding most frequent .."
    kmer_frequencies = count_frequencies(mutated_kmers)
    max_val = kmer_frequencies.values.max
    puts kmer_frequencies.to_a.sort{|a,b| a[1] <=> b[1]}
    good_kmers = kmer_frequencies.to_a.select{|k| k[1] == max_val}.map{|k| k[0]}
    puts good_kmers.inspect
  end

end

lines = File.open("1g/extra_dataset.txt").readlines
#lines = File.open("1g/rosalind_1g.sample.txt").readlines
#lines = File.open("1g/rosalind_1g.txt").readlines

text = lines[0].strip
k, d = lines[1].strip.split(" ").map(&:to_i)

g = Genome.new(text,k,d)
g.find_most_frequent

if DO_TESTS
  # TESTS
  mutations = g.mutate_single("A", 1)
  raise "#{mutations.inspect} - wrong size: #{mutations.size} should be 4" unless mutations.size == 4
  ["A","C","G","T"].each do |b|
    raise "#{mutations.inspect} - missing mutation: #{b}" unless mutations.include?(b)
  end

  mutations = g.mutate_single("AC", 1)
  raise "#{mutations.inspect} - wrong size: #{mutations.size} should be 7" unless mutations.size == 7
  ["AA","AC","AG","AT","GC","TC","CC"].each do |b|
    raise "#{mutations.inspect} - missing mutation: #{b}" unless mutations.include?(b)
  end

  mutations = g.mutate_single("AC", 2)
  raise "#{mutations.inspect} - wrong size: #{mutations.size} should be 16" unless mutations.size == 16
  ["AA","AC","AG","AT","GC","TC","CC"].each do |b|
    raise "#{mutations.inspect} - missing mutation: #{b}" unless mutations.include?(b)
  end
end
