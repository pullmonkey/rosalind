class Genome
  attr_reader :counts

  def initialize(text, k, d)
    @text = text
    @k = k
    @d = d
    @bases = ["A","C","G","T"]
    @counts = {}
    @original_mutators = []
    puts "I: Created Genome with #{text.size} text size, k=#{k} and d=#{d}"
  end

  def mutate(kmers, num_mutations)
    puts "M: mutating our kmers at num mutations: #{num_mutations}"
    kmers.map do |kmer|
      mutate_single(kmer, num_mutations)
    end.flatten.uniq
  end

  def mutate_single(kmer, num_mutations, original_kmer=nil)
    if num_mutations == 0
      @counts[kmer] ||= 0
      @counts[kmer] += 1
      return [kmer] 
    end
    @original_mutators = [] if original_kmer.nil?
    mutations = []
    kmer.size.times do |pos_to_mutate|
      @bases.each do |base|
        arr = kmer.split("")
        arr[pos_to_mutate] = base
        mutation = arr.join("")
        unless @original_mutators.include?(mutation)
          @counts[mutation] ||= 0
          @counts[mutation] += 1
          @original_mutators << mutation
        end
        mutations << mutation
      end
    end
    return mutations if num_mutations == 1
    mutations.map{|m| mutate_single(m, num_mutations - 1, original_kmer || kmer)}.flatten
  end

  def base_kmers
    return @base_kmers unless @base_kmers.nil?
    puts "BK: generating base kmers ..."
    @base_kmers = []
    (0..@text.size-@k).each do |pos|
      @base_kmers << @text[pos..(pos + @k-1)]
    end
    return @base_kmers
  end

  def mutate_kmers
    puts "MK: mutating our base kmers ..."
    mutate(base_kmers, @d)
  end

  def find_most_frequent
    puts "FMF: finding most frequent .."
    mutate_kmers
    max_val = @counts.values.max
    puts @counts.to_a.sort{|a,b| a[1] <=> b[1]}
    good_kmers = @counts.to_a.select{|k| k[1] == max_val}.map{|k| k[0]}
    puts good_kmers.inspect
  end

end

#lines = File.open("sample.txt").readlines
# CATG GCAT
lines = File.open("rosalind_1a.txt").readlines

text = lines[0].strip
k    = lines[1].strip.to_i
d    = 0

g = Genome.new(text,k,d)
g.find_most_frequent
