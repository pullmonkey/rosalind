#lines = File.open("sample.txt").readlines.map(&:strip)
lines = File.open("rosalind_cons.txt").readlines.map(&:strip)

def profile_matrix(lines)
  dnas = []
  full_line = ""
  lines.each do |line|
    if line =~ /^>/
      dnas << full_line unless full_line.empty?
      full_line = ""
      next
    else
      full_line += line
    end
  end
  dnas << full_line

  pm = {
    "A" => [0]*dnas.last.size,
    "C" => [0]*dnas.last.size,
    "G" => [0]*dnas.last.size,
    "T" => [0]*dnas.last.size
  }

  dnas.each do |dna|
    puts dna.inspect
    dna.split("").each_with_index do |d,i|
      pm[d][i] += 1
    end
  end
  pm
end

def consensus(profile_matrix)
  to_ret = ""
  letters = ["A","C","G","T"]
  profile_matrix.values.last.size.times do |x|
    to_ret += letters[profile_matrix.values.map{|k| k[x]}.each_with_index.max.last]
  end
  to_ret
end

pm = profile_matrix(lines)
c  = consensus(pm)

puts c
pm.each do |k,v|
  puts "#{k}: #{v.join(" ")}"
end
puts ""
