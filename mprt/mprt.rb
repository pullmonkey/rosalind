require 'open-uri'
require 'nokogiri'

#prots = File.open("sample.txt").readlines.map(&:strip)
prots = File.open("rosalind_mprt.txt").readlines.map(&:strip)

prots.each do |prot|
  doc = Nokogiri::HTML(open("http://www.uniprot.org/uniprot/#{prot}"))
  tds = doc.css("#content-features td")
  locations = []
  tds.each do |td|
    next if td.content !~ /N-linked/
    #locations << td.parent.children[2].content
  end
  if locations.empty?
    fasta = ""
    open("http://www.uniprot.org/uniprot/#{prot}.fasta") do |f|
      f.each_line do |line| 
        next if line =~ /^>/
        fasta += line.strip 
      end
    end
    # N{P}[ST]{P}
    (fasta.size - 4).times do |pos|
      if fasta[pos..pos+3] =~ /N[^P][ST][^P]/
        locations << pos + 1
      end
    end
  end
  unless locations.empty?
    puts prot
    puts locations.join(" ")
  end
end
