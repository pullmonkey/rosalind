class Genome
  attr_reader :counts

  def initialize(text, k, d)
    @text = text
    @k = k
    @d = d
    @bases = ["A","C","G","T"]
    @counts = {}
    @original_mutators = []
    puts "I: Created Genome with #{text.size} text size, k=#{k} and d=#{d}"
  end

  def mutate(kmers, num_mutations, opts={})
    puts "M: mutating our kmers at num mutations: #{num_mutations}"
    kmers.map do |kmer|
      mutate_single(kmer, num_mutations, opts)
    end.flatten.uniq
  end

  def mutate_single(kmer, num_mutations, opts={})
    original_kmer = opts[:original_kmer]
    @original_mutators = [] if original_kmer.nil?
    mutations = []
    kmer.size.times do |pos_to_mutate|
      @bases.each do |base|
        arr = kmer.split("")
        arr[pos_to_mutate] = base
        mutation = arr.join("")
        unless @original_mutators.include?(mutation)
          to_count = mutation
          # count for the reverse of it if we are in reverse mode
          to_count = reverse_complement(mutation) if opts[:reverse]
          @counts[to_count] ||= 0
          @counts[to_count] += 1
          @original_mutators << mutation
          mutations << mutation
        end
      end
    end
    return mutations if num_mutations == 1
    mutations.map{|m| mutate_single(m, num_mutations - 1, opts.merge(:original_kmer => (original_kmer || kmer)))}.flatten
  end

  def reverse_complement(str)
    str.reverse.gsub("A", "t").gsub("T","a").gsub("G","c").gsub("C","g").upcase
  end

  def base_kmers(opts={})
    return @base_kmers unless @base_kmers.nil?
    puts "BK: generating base kmers ..."
    @base_kmers = []
    (0..@text.size-@k).each do |pos|
      kmer = @text[pos..(pos + @k-1)]
      @base_kmers << kmer
      if opts[:reverse]
        @reverse_comp_kmers ||= []
        @reverse_comp_kmers << reverse_complement(kmer)
      end
    end
    return @base_kmers
  end

  def mutate_kmers(opts={})
    puts "MK: mutating our base kmers ..."
    mutate(base_kmers(opts), @d)
    if opts[:reverse]
      mutate(@reverse_comp_kmers, @d, opts)
    end
  end

  def find_most_frequent(opts={})
    puts "FMF: finding most frequent .."
    mutate_kmers(opts)
    max_val = @counts.values.max
    puts @counts.to_a.sort{|a,b| a[1] <=> b[1]}
    good_kmers = @counts.to_a.select{|k| k[1] == max_val}.map{|k| k[0]}
    puts good_kmers.inspect
  end

end

#lines = File.open("sample.txt").readlines
# ["ATGT", "ACAT"]
lines = File.open("extra.txt").readlines
# AGCGCCGCT AGCGGCGCT
#lines = File.open("rosalind_1h.txt").readlines

text = lines[0].strip
k, d = lines[1].strip.split(" ").map(&:to_i)

g = Genome.new(text,k,d)
g.find_most_frequent(:reverse => true)
#puts g.counts["AGCGCCGCT"]
#puts g.counts["AGCGGCGCT"]
puts g.counts["ATGT"]
puts g.counts["ACAT"]
