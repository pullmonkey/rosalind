n = ARGV[0].to_i

perms = (1..n).to_a.permutation
puts perms.size
perms.each do |perm|
  puts perm.join(" ")
end
