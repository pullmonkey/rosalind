require 'active_support/all'

#rna = File.open("sample.txt").readlines[0].strip
rna = File.open("rosalind_prot.txt").readlines[0].strip

mapping_data = %|UUU F      CUU L      AUU I      GUU V
UUC F      CUC L      AUC I      GUC V
UUA L      CUA L      AUA I      GUA V
UUG L      CUG L      AUG M      GUG V
UCU S      CCU P      ACU T      GCU A
UCC S      CCC P      ACC T      GCC A
UCA S      CCA P      ACA T      GCA A
UCG S      CCG P      ACG T      GCG A
UAU Y      CAU H      AAU N      GAU D
UAC Y      CAC H      AAC N      GAC D
UAA Stop   CAA Q      AAA K      GAA E
UAG Stop   CAG Q      AAG K      GAG E
UGU C      CGU R      AGU S      GGU G
UGC C      CGC R      AGC S      GGC G
UGA Stop   CGA R      AGA R      GGA G
UGG W      CGG R      AGG R      GGG G|

mappings = Hash[*mapping_data.split(/\s{2,}|\n/).map{|x| x.split(" ")}.flatten]

puts rna.split("").in_groups_of(3).map(&:join).map{|s| mappings[s]}.join("").gsub("Stop","")
