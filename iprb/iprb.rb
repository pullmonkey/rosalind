#k, m, n = File.open("sample.txt").readlines[0].strip.split(" ").map(&:to_f)
k, m, n = File.open("rosalind_iprb.txt").readlines[0].strip.split(" ").map(&:to_f)

def probability_dominant(k,m,n)
  population = k + m + n
  # k is dominant YY
  # m is hetero Yy
  # n is recessive yy
  # possible outcomes
  # YY with YY => 1.0
  # YY with Yy => 1.0
  # YY with yy => 1.0
  # Yy with YY => 1.0
  # Yy with Yy => 0.75
  # Yy with yy => 0.25
  # yy with YY => 1.0
  # yy with Yy => 0.25
  # yy with yy => 0.00
  dom_org1 = k / population 
  het_org1 = m / population 
  rec_org1 = n / population 
  # probability given dominant homo for first random org
  dom  =  k / population
  # probability given hetero for first random org
  dom += (m / population) * ((k + 0.75 * (m - 1) + 0.25 * n) / (population - 1))
  # probability given recessive for first random org
  dom += (n / population) * ((k + 0.75 * m) / (population - 1))
end

puts probability_dominant(k,m,n)
