#lines = File.open("sample.txt").readlines.map(&:strip)
lines = File.open("rosalind_hamm.txt").readlines.map(&:strip)

def hamming_distance(s, t)
  to_ret = 0
  split_t = t.split("")
  s.split("").each_with_index do |a,i|
    to_ret += 1 if a != split_t[i]
  end
  to_ret
end

puts hamming_distance(lines[0], lines[1])
