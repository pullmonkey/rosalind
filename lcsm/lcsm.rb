#lines = File.open("sample.txt").readlines.map(&:strip)
lines = File.open("rosalind_lcsm.txt").readlines.map(&:strip)

def common_for(dna1,dna2,common_substrings)
  sub_strings = []
  if common_substrings.empty?
    # if common_substrings is empty, then we need to search ALL possibilities
    # use shorter string for length sliding window
    short_dna, long_dna = dna1.size > dna2.size ? [dna1,dna2] : [dna2,dna1]
    (2..short_dna.size).each do |len|
      (0..short_dna.size-len).each do |i|
        if long_dna =~ /(#{short_dna[i..i+len-1]})/
          sub_strings << $1
        end
      end
    end
  else
    # otherwise, just what we found in common, and clear out as we go
    common_substrings.each do |cs|
      sub_strings << cs if dna1 =~ /#{cs}/ and dna2 =~ /#{cs}/
    end
  end
  return sub_strings
end

common_substrings = []
dnas = []
full_line = ""
lines.each do |line|
  if line =~ /^>/
    unless full_line.empty?
      # look for commons between this dna and last one
      last_dna = dnas.last
      if last_dna
        common_substrings = common_for(last_dna,full_line, common_substrings)
      end
      dnas << full_line 
    end
    full_line = ""
  else
    full_line += line
  end
end
last_dna = dnas.last
common_substrings = common_for(last_dna,full_line, common_substrings)
dnas << full_line

puts common_substrings.sort{|a,b| a.size <=> b.size}.last
