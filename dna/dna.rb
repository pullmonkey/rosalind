#dna = File.open("sample.txt").readlines[0].strip
dna = File.open("rosalind_dna.txt").readlines[0].strip
counts = {}
dna.split("").each{|d| counts[d] ||= 0; counts[d] += 1}
puts "#{counts["A"]} #{counts["C"]} #{counts["G"]} #{counts["T"]}"
