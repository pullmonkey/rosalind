#s,t = File.open("sample.txt").readlines.map(&:strip)
s,t = File.open("rosalind_subs.txt").readlines.map(&:strip)

def find_matches(s,t)
  full_matches = s.enum_for(:scan, t).map{|s| Regexp.last_match.begin(0)}
  # now scan around full matches for length of t
  # in case there are nestings
  orig_full_matches = full_matches.clone
  orig_full_matches.each_with_index do |fm,i|
    full_matches << s[(fm+1)..((orig_full_matches[i+1] || -1) - t.size + 1)].enum_for(:scan, t).map{|s| Regexp.last_match.begin(0) + fm + 1}
  end
  full_matches.flatten.compact.sort.map{|x| x + 1}
end

puts find_matches(s,t).join(" ")
