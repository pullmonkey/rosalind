lines = File.open("1f/rosalind_1f.txt").readlines

pattern = lines[0].strip
text    = lines[1].strip
d       = lines[2].strip.to_i

good_positions = []
pattern_length = pattern.size
previous_num_mismatches = 0
(0..text.size-pattern_length).each do |pos|
  to_compare = text[pos..(pos + pattern_length-1)]
  num_mismatches = 0
  if previous_num_mismatches > 100 + d + 2
    num_mismatches = previous_num_mismatches - 1
  elsif pattern == to_compare
    good_positions << pos 
  else
    pattern.split("").each_with_index do |p,i|
      num_mismatches += 1 if p != to_compare[i..i] 
    end
    good_positions << pos if num_mismatches <= d
  end
  previous_num_mismatches = num_mismatches
end

puts good_positions
