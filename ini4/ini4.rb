require 'active_support/all'
file = ARGV[0]

a,b = File.open(file).readlines[0].split(" ").map(&:to_i)

puts (a..b).select{|x| x.odd?}.sum
